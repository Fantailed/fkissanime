function getVidUrl() {
    // Gets source parameter from video tag on site
    var videoTags = document.getElementsByTagName('video');
    for( var i = 0; i < videoTags.length; i++ ){
        if (videoTags.item(i)) {
            return videoTags.item(i).currentSrc;
        }
    }
}

function goToAdFree() {
    next_eps = document.getElementById("btnNext");
    if (next_eps) {
        // Store next episode URL if current episode is not the last
        next_eps = next_eps.parentNode.href;
        browser.storage.local.set({"next_eps": next_eps});
        window.location.href = vid_url;
    } else {
        console.log("Next eps was null");
        browser.storage.local.remove("next_eps");
        window.location.href = vid_url;
    }
}

// ======== MAIN =========

var next_eps = null;
var vid_url = getVidUrl();

if (window.location.href.indexOf("kissanime") > -1) {
    let action = setTimeout(() => {
        // Place button to go to clean video
        var button = document.createElement("button");
        button.innerHTML = "<b>GO TO AD-FREE VERSION</b>";
        button.style = `position: fixed;
                        z-index: 9999;
                        top: 50%;
                        left: 0;
                        color: white;
                        background-color: #588B02;
                        border: none;
                        height: 75px;
                        width: 200px;`;
        button.onclick = goToAdFree;
        document.body.prepend(button);
    }, 250);
} else {    // Not on kissanime but on external video site, most likely googlevideos
    browser.storage.local.get("next_eps").then(
        (next_url) => {
            // Next episode "(>)" icon
            var next_img = document.createElement('img');
            next_img.src = browser.runtime.getURL("icons/next.png")
            next_img.style.height = "40px";
            next_img.style.width = "auto";

            if (next_url["next_eps"]) {
                // Make next video button clickable if there is a next episode
                var next = document.createElement('a');
                next.href = next_url["next_eps"];
                next.title = "Next episode";
                next.style= "position:absolute;right:50px;bottom:50px;";
                next.appendChild(next_img);
                document.body.appendChild(next);
            } else {
                // Else make it gray
                next_img.title = "No more episodes :(";
                next_img.style.position = "absolute";
                next_img.style.right = "50px";
                next_img.style.bottom = "50px";
                next_img.style.filter = "grayscale(100%)"
                document.body.appendChild(next_img);
            }
            
            // Resize Video
            var video = document.getElementsByTagName('video')[0];
            video.style.height = "80%";
        }
    )
}
