# ![logo](icons/icon.png "FKissAnime") FKissAnime

This extension ads a "Go to ad-free version" button to KissAnime's video sites.  
This button redirects you to the pure video source.
No ads, no bullshit, just the pure video window and a small "Next Episode"
button in the corner for your convenience.

Note that for this extension to work, you need to fill out the captchas on the
actual site and get the actual video player loaded first.  
KissAnime generates a unique and time-limited link for you that I don't know
how to get otherwise (if you know how, feel free to share!).  
Note that this time limit holds for the pure video source as well, so you can't
pause it forever and come back and continue watching without getting a new link
from KissAnime first, but you should have more than enough time to finish one episode.

It does allow you to download though!
